db.fruits.aggregate([
// $match is similar to find(). You can use query operators to make your criteria more flexible
/*
$match -> Apple, Kiwi, Banana
*/
{ $match: { onSale: true }},
//  $group - allows us to group together documents and create an analysis out of the group elements
// _id: $supplier_id
/*
Apple = 1.0
Kiwi = 1.0
Banana = 2.0

_id: 1.0
	Apple, Kiwi

	total: sum of the fruit stock of 1.0
	total: Apple stocks + Kiwi stocks
	total: 20 			+	25
	total: 45

_id: 2.0
	Banana

	total: sum of the fruit stock of 2.0
	total: Banana stocks
	total: 15
*/
// $sum - used to add or total the values in a given field
{ $group: {_id: "$supplier_id", total: { $sum: "$stock"}}}
]);

db.fruits.aggregate([
	/*
	$match - Apple, Kiwi, Banana
	*/
	{ $match: { onSale: true }},
	/*
	Apple - 1.0
	Kiwi - 1.0
	Banana - 1.0

	_id: 1.0
		avgStocks: average stocks of the fruit in 1.0
		avgStocks: (Apple Stocks + Kiwi Stocks) /2
		avgStocks: (20			+ 25) /2
		avgStocks: 45 / 2
		avgStocks: 22.5

		_id: 2.0
		avgStocks: average stocks of fruits in 2.0
		avgStocks: Banana Stocks / 1
		avgStocks: 15 / 1
		avgStocks: 15
	*/
	/*
		{
		_id: 1,
		avgStocks: 22.5
		}
		{
		_id: 2,
		avgStocks: 15
		}
	*/
	// $avg - gets the average of the values of the given field per group
	{ $group: {_id: '$supplier_id', avg Stocks: { $avg: '$stock'}}},
	// $project - can be used when "aggregating" data to include/ exclude fields from the returned (Field Projection)
	/*
		{
		avgStocks: 22.5
		}
		{
		avgStocks: 15
		}
	*/
	{ $project: {_id: 0}}
	]);

db.fruits.aggregate([
  { $match: {onSale:true}},
  { $group: {_id: '$supplier_id', maxPrice: { $max: '$price'}}},
  { $sort: { maxPrice: -1}}
]);
db.fruits.aggregate([
  { $unwind: "$origin"}
]);
db.fruits.aggregate([
  { $unwind: '$origin'},
  { $group: { _id: '$origin', kinds: {$sum: 1}}}
]);

