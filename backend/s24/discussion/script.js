// [SECTION Arrays and Indexes]
let grades =[98.5, 94.3, 89.2, 90.1];
let computer_brands = ["Acer", "Asus", "lenovo", "Neo", "Redfox", "Getaway", "Toshiba", "Fujitsu"];
let mix_array = [12, "Asus", null, undefined, {}];

// Alternative way to write arrays
let my_tasks = [
	"drink html",
	"eat javascript",
	"inhale CSS",
	"bake sass"
	];

// Reassigning values
console.log("Array before reassignment");
console.log(my_tasks);

my_tasks[0] = "run hello world";
console.log("Array after reassignment");
console.log(my_tasks);


// [SECTION] Reading from arrays
console.log(computer_brands[1]);
console.log(grades[3]);

// Getting the Length of an array
console.log(computer_brands.length);

// Accessing Last element in an array
let index_of_last_element = computer_brands.length - 1;

console.log(computer_brands[index_of_last_element]);


//  [SECTION]Array Methods
let fruits = ["Apple", "Orange", "Kiwi", "Passionfruit"];

//  Push Method
console.log("Current array:")
console.log(fruits);

fruits.push("Mango", "Cocomelon");

console.log("Updated array after push method:")
console.log(fruits);

// Pop Method
console.log("Current array:")
console.log(fruits);

let removed_item = fruits.pop();

console.log("Updated array after pop method:")
console.log(fruits);
console.log("Removed fruit: " + removed_item);

// Unshift Method
console.log("Current array:")
console.log(fruits);

fruits.unshift("Lime", "Star Apple");

console.log("Updated array after unshift method:")
console.log(fruits);

// Shift Method
console.log("Current array:")
console.log(fruits);

fruits.shift("Lime", "Star Apple");

console.log("Updated array after shift method:")
console.log(fruits);

// Splice Method
console.log("Current array:")
console.log(fruits);

fruits.splice(1, 2, "Lime", "Cherry");

console.log("Updated array after splice method:")
console.log(fruits);

// Sort Method
console.log("Current array:")
console.log(fruits);

fruits.sort();

console.log("Updated array after sort method:")
console.log(fruits);

let index_of_lenovo = computer_brands.indexOf("Lenovo");
console.log("The index of lenovo is:" + index_of_lenovo);

let index_of_lenovo_from_last_item = computer_brands.lastindexOf("Lenovo");
console.log("The index of lenovo starting from the end of the array is:" + index_of_lenovo_from_last_item);

let last_index_start = computer_brands.lastIndexOf("Lenovo", 5);