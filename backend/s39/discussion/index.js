console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

fetch('https://jsonplaceholder.typicode.com/posts')
		.then(response => response.json()) // Converts the JSON string from the response into regular javascript format
		.then(posts => console.log(posts));


// As of ES6, the async/aait suntax can be used to replace the .then() convention as it helps a function
async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

	let json_result = await result.json();

	console.log(json_result);
}

fetchData();

// adding headers, body and method to the fetch() function
fetch('https://jsonplaceholder.typicode.com/posts', {
	method:'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "New post!",
		body: "Hello World,",
		userId: 2
	})
})
.then(response => response.json())
.then(created_posts => console.log(created_posts));


// UPDATING EXISTING POST
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method:'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Corrected post!",
	})
})
.then(response => response.json())
.then(updated_posts => console.log(updated_posts));


// DELETING EXISTING POST
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then(response => response.json())
.then(deleted_posts => console.log(deleted_posts));


// FILTERING POSTS
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(response => response.json())
.then(posts => console.log(posts));


// Getting Comments of a Post
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(response => response.json())
.then(comments => console.log(comments));