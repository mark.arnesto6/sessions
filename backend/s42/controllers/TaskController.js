const Task = require('../models/Task.js');

module.exports.getAllTasks = () => {
	// Accessing the 'Task' model and using find() with and empty object as the
	return Task.find({}).then((result, error) => {
		if(error){
			return response.send({
				message: error.message
			})
		}

		return {
			task: result
		}
	})
}

module.exports.createTask = (request_body) => {
	return Task.findOne({name: request_body.name}).then((result) => {
		// Check if the task already exists by utilizing the 'name' property. If it does, then return a response to the user.
		if(result != null && result.name == request_body.name){

			return { message: "Duplicate task found!"};

		} else {
			//  1. Create a new instance of the task model which will contain that properties required based on the schema
			let newTask = new Task({
				name: request_body.name
			});

			// 2. Save the new task to the databse
		return newTask.save().then((saveTask, error) => {
				if(error){
					return {
						message: error.message
					};
				}

				return {  
					message: 'New task created'
				};
			})
		}
	})
}