// Mongosh

db.users.insert.users.insertOne({
	firstname: "Jane",
	lastname: "Doe",
	age: 21,
	contact: {
	phone: "0912345678",
	email: "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "Phyton"],
department: "none"
});

// Inserting multiple documents at once
db.users.insertMany([
		{
		"firstName": "John",
		"lastName": "Doe"
		},
		{
		"firstName": "Joseph",
		"lastName": "Doe"
		}
	]);

// [SECTION] Retrieving documents
// Retrieving all the inserted users
db.users.find();

// Retrieving a specific document from a collection
db.users.find({ "firstName": "John" });

// [SECTION] Updating existing documents
// For updating a single document
db.users.updateOne(
	// First part - retrieves a specific user using the ID
	{
		"_id": ObjectId("64c1c28a1ab0a60e21a4d58f")
	},
	// Second part - uses the $set keyword to set a specific property of that user to a new value
	{
		$set: {
			"lastName": "Gaza"
		}
	}
);

// For updating multiple documents
// updateMany() allows the modification of 2 or more documents as compared to updateOne() which can only accomodate a single document.
db.users.updateMany(
	{
		"lastName": "Doe"
	},
	{
		$set: {
			"firstName": "Mary"
		}
	}

);


// [SECTION] Deleting documents from a collection
// Deleting multiple documents
db.users.deleteMany({ "lastName": "Doe" });

// Deleting a single document
db.users.deleteOne({
	"_id": ObjectId("64c1c28a1ab0a60e21a4d58f")
});

