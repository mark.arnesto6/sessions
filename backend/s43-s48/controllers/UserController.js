const User = require('../models/User.js');
const Shirt = require('../models/Shirt.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then((result, error) => {
		if(error){
			return{
				message: error.message
			}
		}

		if(result.length <= 0) {
			return false;
		}

		return true;
	})
}

module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) => {
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully registered a user!'
		};
	}).catch(error => console.log(error));
}

module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		if(result == null) {
			return response.send({
				message: "The user isn't registered yet."
			})
		}

		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			return response.send({accessToken: auth.createAccessToken(result)});
		} else{
			return response.send({
				message: 'Your password is incorrect'
			})
		}
	}).catch(error => response.send(error));
}
module.exports.getProfile = (request_body) => {
    return User.findOne({ _id: request_body.id}).then((user, error) => {
    	if(error) {
        return {
            message: error.message
        	}
      	}

       	 user.password = "";
      	
    
    	return user;
	}).catch(error => console.log(error));
}

module.exports.enroll = async (request, response) => {
	// Blocks this function if the user is an admin
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}


	let isUserUpdated = await User.findById(request.user.id).then(user => {

		let new_enrollment = {
			shirtId: request.body.shirtId
		}

		user.enrollments.push(new_enrollment);

		return user.save().then(updated_user => true).catch(error => error.message);
	})

	if(isUserUpdated !== true){
		return response.send({ message: isUserUpdated});
	}

	let isShirtUpdated = await Shirt.findById(request.body.shirtId).then(shirt => {
		let new_enrollee = {
			userId: request.user.id
		}

		shirt.enrollees.push(new_enrollee);

		return shirt.save().then(updated_shirt => true).catch(error => error.message)
	})

	if(isShirtUpdated !== true){
		return response.send({ message: isShirtUpdated });
	}
	
	if(isUserUpdated && isShirtUpdated){
	return response.send ({ message: 'Enrolled successfully'});
		}
}

module.exports.getEnrollments = (request, response) => {
	User.findById(request.user.id)
	.then(result => response.send(result.enrollments))
	.catch(error => response.send(error.message))
}