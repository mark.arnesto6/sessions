const Shirt = require('../models/Shirt.js');

module.exports.addShirt = (request, response) => {
  let new_shirt = new Shirt({
    name: request.body.name,
    description: request.body.description,
    price: request.body.price
  });

  return new_shirt.save().then((saved_shirt, error) => {
    if(error){
      return response.send(false);
    }

    return response.send(true);
  }).catch(error => response.send(error));
}

module.exports.getAllShirts = (request, response) => {
  return Shirt.find({}).then(result => {
    return response.send(result);
  })
}

module.exports.getAllActiveShirts = (request, response) => {
  return Shirt.find({isActive: true}).then(result => {
    return response.send(result);
  })
}

module.exports.getShirt = (request, response) => {
  return Shirt.findById(request.params.id).then(result => {
    return response.send(result);
  })
}

module.exports.updateShirt = (request, response) => {
  let updated_shirt_details = {
    name: request.body.name,
    description: request.body.description,
    price: request.body.price
  };

  return Shirt.findByIdAndUpdate(request.params.id, updated_shirt_details).then((shirt, error) => {

    if(error){
      return response.send({
        message: error.message
      })
    }

    return response.send({
      message: 'Shirt has been updated successfully!'
    })
  })
}

module.exports.archiveShirt = (request, response) => {
  return Shirt.findByIdAndUpdate(request.params.id, { isActive: false }).then((shirt, error) => {
    if(error){
      return response.send(false);
    }

    return response.send(true);
  })
}

module.exports.activateShirt = (request, response) => {
  return Shirt.findByIdAndUpdate(request.params.id, { isActive: true }).then((shirt, error) => {
    if(error){
      return response.send(false);
    }

    return response.send(true);
  })
}


module.exports.searchShirts = (request, response) => {
  const shirtName = request.body.shirtName;
  return Shirt.find({ name: { $regex: shirtName, $options: 'i' } })
    .then(shirts => {
      response.send(shirts);
    })
    .catch(error => {
      response.send({
        message: error.message
      });
    });
};
