// Server variables
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config()
const port = 4000;
const userRoutes = require('./routes/userRoutes.js');
const shirtRoutes = require('./routes/shirtRoutes')
const app = express();


// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors()); // This wiwll allow our hosted front-end app to send request to this server

// Routes
app.use('/api/users', userRoutes);
app.use('/api/shirts', shirtRoutes);

// Database connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-arnesto.srjvq71.mongodb.net/b303-printing-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.on('error', () => console.log("Can't connect to database."))
mongoose.connection.once('open', () => console.log('Connected to MongoDB!'));


app.listen(process.env.PORT || port, () => {
	console.log(`Printing System API is now running at localhost:${process.env.PORT || port}`);
})

module.exports = app;