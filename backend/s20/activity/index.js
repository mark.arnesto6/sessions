function getUserInfo() {
  return {
    name: "John Doe",
    age: 25,
    address: "123 Street, Quezon City",
    isMarried: false,
    petName: "Danny"
  };
}

function getArtistsArray() {
  return ["Ben & Ben", "Arthur Nery", "Linkin Park", "Paramore", "Taylor Swift"];
}

function getSongsArray() {
  return ["Kathang Isip", "Binhi", "In the End", "Bring by Boring Brick", "Love Story"];
}

function getMoviesArray() {
  return ["The Lion King", "Meet the Robinsons", "Howl's Moving Castle", "Tangled", "Frozen"];
}

function getPrimeNumberArray() {
  return [2, 3, 5, 7, 17];
}

const userInfo = getUserInfo();
console.log(userInfo);

const favoriteArtists = getArtistsArray();
console.log(favoriteArtists);

const favoriteSongs = getSongsArray();
console.log(favoriteSongs);

const favoriteMovies = getMoviesArray();
console.log(favoriteMovies);

const primeNumbers = getPrimeNumberArray();
console.log(primeNumbers);



try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}