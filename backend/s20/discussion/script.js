// Function Declaration & Invocation
// function printName(){
// 	console.log("My name is Jeff");
// }

// printName();

// //  Function expression
// let variable_function = function() {
// 	console.log("Hello from function expression!")
// }

// variable_function();

// Scoping
let global_variable = " Call me mr. worldwide";

console.log(global_variable);

function showNames(){
	let function_variable ="Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);
	console.log(global_variable);
}

showNames();

// NESTED FUNCTIONS
function parentFunction(){
	let name = "Jane";

	function childFunction(){
		let nested_name = "John"

		console.log(name);
		console.log(nested_name);
	}

	childFunction();

	// console.log(nested_name);

}

parentFunction();

// childFunction();

function printWelcomeMessageForUser(){
	let first_name = prompt("Enter your first name: ");
	let last_name = prompt("Enter your last name: ");

	console.log("Hello, " + first_name + " " + last_name + "!");
	console.log("Welcome sa page ko!");
}

printWelcomeMessageForUser();

// Return Statement
function fullName(){
	return "Earl Rafael Diaz";
}

console.log(fullName());