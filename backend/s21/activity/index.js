function addNum(num1, num2) {
  return num1 + num2;
}

function subNum(num1, num2) {
  return num1 - num2;
}

let sum = addNum(5, 15);

console.log("Sum of 5 and  15:");
console.log(sum)

let difference = subNum(20, 5);

console.log("Difference of 20 and 5:");
console.log(difference);

function multiplyNum(num1, num2) {
  return num1 * num2;
}

function divideNum(num1, num2) {
  return num1 / num2;
}

let product = multiplyNum(50, 10);
console.log("The product of 50 and 10:");
console.log(product);

let quotient = divideNum(50, 10);
console.log("The quotient of 50 and 10:");
console.log(quotient);


function getCircleArea(radius) {
  return Math.PI * radius ** 2;
}

let circleArea = getCircleArea(15);
console.log("The result of getting area of a circle with 15 radius:")
console.log(circleArea.toFixed(2));

function getAverage(num1, num2, num3, num4) {
  return (num1 + num2 + num3 + num4) / 4;
}

let averageVar = getAverage(20, 40, 60, 80);
console.log("The average of 20, 40, 60, 80:")
console.log(averageVar);

function checkIfPassed(score, totalScore) {
  let percentage = (score / totalScore) * 100;
  let passingPercentage = 75;
  let isPassed = percentage > passingPercentage;
  return isPassed;
}

let isPassingScore = checkIfPassed(38, 50);
console.log("Is 38/50 a passing score:")
console.log(isPassingScore);


try{
  module.exports = {

    addNum: typeof addNum !== 'undefined' ? addNum : null,
    subNum: typeof subNum !== 'undefined' ? subNum : null,
    multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
    divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
    getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
    getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
    checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

  }
} catch(err){

}