// Server varialbles for initialization
const express = require('express'); // Imports express
const app = express(); // Initializes express
const port = 4000;

// Middleware
app.use(express.json()); // Registering a middleware that will make express be able to read JSON format from requests
app.use(express.urlencoded({extended: true})); // A middleware that will allow express to be able to read data types other that the default string and array it can usually read

// Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}`))


// [SECTION] Routes
app.get('/', (request, response) => {
	response.send('Hello World!');
})

app.post('/greeting', (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`);
})

let users = [];

app.post('/register', (request, response) => {
	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);

		response.send(`User ${request.body.username} has successfully been registered!`);
	} else {
		response.send('Please input Both username AND password.');
	}
})

// Gets the list/array of users
app.get('/users', (request, response) => {
	response.send(users);
})

app.delete('/delete-user', (request, response) => {
  
  if (request.body.username !== 0) {

    response.send(`User ${request.body.username} has been deleted.`);
  } else {
  	 response.send('User does not exist.');
  }
});



module.exports = app;