import { Card, Button } from "react-bootstrap";
import PropTypes from 'prop-types';
import { useState } from 'react';
import {Link} from 'react-router-dom';

export default function CourseCard({course}) {

    const {_id, name, description, price} = course;

  return (
        <Card className="cardHighlight">
          <Card.Body>
            <Card.Title> {name} </Card.Title>

            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>

            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>Php{price}</Card.Text>

            <Link className="btn btn-primary" to={`/courses/${_id}`}>View Details</Link>
          </Card.Body>
        </Card>
  )
}

CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
}
