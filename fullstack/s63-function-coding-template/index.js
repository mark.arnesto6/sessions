function countLetter(letter, sentence) {
    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined;
    }

    let result = 0;
    for (let index = 0; index < sentence.length; index++) {
        if (sentence[index] === letter) {
            result++;
        }
    }
    
    return result;
}


function isIsogram(text) {
    const lowerCaseText = text.toLowerCase();
    
    for (let index = 0; index < lowerCaseText.length; index++) {
        for (let j = index + 1; j < lowerCaseText.length; j++) {
            if (lowerCaseText[index] === lowerCaseText[j]) {
                return false;
            }
        }
    }
    
    return true;
}


function purchase(age, price) {
    if (age < 13) {
        return undefined;
    } else if (age >= 13 && age <= 21 || age >= 65) {
        const discountedPrice = price * 0.8;
        return discountedPrice.toFixed(2);
    } else {
        return price.toFixed(2);
    }
}


function findHotCategories(items) {
    const categoriesWithNoStocks = {};

    for (const item of items) {
        if (item.stocks === 0) {
            categoriesWithNoStocks[item.category] = true;
        }
    }

    return Object.keys(categoriesWithNoStocks);
}



function findFlyingVoters(candidateA, candidateB) {
    const flyingVoters = [];

    for (const voterA of candidateA) {
        for (const voterB of candidateB) {
            if (voterA === voterB) {
                flyingVoters.push(voterA);
                break;
            }
        }
    }

    return flyingVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};